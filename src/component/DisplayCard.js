import {
  IosShare,
  KeyboardArrowDownOutlined,
  Launch,
  MoreHoriz,
} from "@mui/icons-material";
import React from "react";
import "../App.css";

export default function DisplayCard({ imageProp }) {
  const { image } = imageProp;
  return (
    <div className="card-container">
      <img src={image} className="card-image" alt="..." />
      <div className="card-overlay"></div>
      <button className="button-overlay-one">
        My Saves <KeyboardArrowDownOutlined />
      </button>
      <button className="button-overlay-two">Save</button>
      <div className="bottom-button-overlay">
        <div className="bottom-button-container">
          <button className="bottom-button-unique">
            <Launch /> ronie.timbal
          </button>
          <button className="bottom-button">
            <IosShare />
          </button>
          <button className="bottom-button">
            <MoreHoriz />
          </button>
        </div>
      </div>
    </div>
  );
}
