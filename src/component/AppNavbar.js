import { faCommentDots } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  AccountCircle,
  KeyboardArrowDown,
  Notifications,
  Pinterest,
  Search,
} from "@mui/icons-material";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import "../App.css";

function AppNavbar() {
  return (
    <div className="appNavbar-container">
      <div className="nav-items">
        <div style={{ color: "red" }}>
          <Pinterest />
        </div>
      </div>
      <div className="nav-items">
        <div className="home-item">Home</div>
      </div>
      <div>
        <div className="dropdown">
          <button className="dropbtn">
            Create <KeyboardArrowDown />
          </button>
          <div id="myDropdown" className="dropdown-content">
            <a href="#create-idea-pin" className="drop-links">
              Create Idea Pin
            </a>
            <a href="#create-pin" className="drop-links">
              Create Pin
            </a>
          </div>
        </div>
      </div>
      <div className="search-bar-container">
        <button className="search-button">
          <Search />
        </button>
        <input
          className="search-bar"
          type="text"
          placeholder="Search.."
        ></input>
      </div>
      <div className="nav-items">
        <div>
          <OverlayTrigger
            placement="bottom"
            overlay={<Tooltip id={`tooltip-bottom`}>Notifications</Tooltip>}
          >
            <Notifications />
          </OverlayTrigger>
        </div>
      </div>
      <div className="nav-items">
        <div>
          <OverlayTrigger
            placement="bottom"
            overlay={<Tooltip id={`tooltip-bottom`}>Messages</Tooltip>}
          >
            <FontAwesomeIcon icon={faCommentDots} />
          </OverlayTrigger>
        </div>
      </div>
      <div className="nav-items">
        <div>
          <OverlayTrigger
            placement="bottom"
            overlay={<Tooltip id={`tooltip-bottom`}>Your Profile</Tooltip>}
          >
            <AccountCircle />
          </OverlayTrigger>
        </div>
      </div>
      <div className="nav-items">
        <div>
          <OverlayTrigger
            placement="bottom"
            overlay={
              <Tooltip id={`tooltip-bottom`}>Accounts and more options</Tooltip>
            }
          >
            <KeyboardArrowDown />
          </OverlayTrigger>
        </div>
      </div>
    </div>
  );
}

export default AppNavbar;
