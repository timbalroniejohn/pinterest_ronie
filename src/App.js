import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import AppNavbar from "./component/AppNavbar";
import PhotoDetails from "./pages/PhotoDetails";

function App() {
  return (
    <div className="App">
      <AppNavbar />
      <PhotoDetails />
    </div>
  );
}

export default App;
