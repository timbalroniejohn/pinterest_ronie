import emoji from "../assets/1F603.svg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  AccountCircle,
  ArrowBack,
  IosShare,
  KeyboardArrowDown,
  MoreHoriz,
  QuestionMarkOutlined,
} from "@mui/icons-material";
import React from "react";
import { faLink } from "@fortawesome/free-solid-svg-icons";
import DisplayCard from "../component/DisplayCard";
import imageData from "../data.js";

export default function PhotoDetails() {
  const displayImage = imageData.map((image) => {
    return <DisplayCard key={image.id} imageProp={image} />;
  });

  return (
    <div className="photoDetails-page-container">
      <div className="arrow-back">
        <ArrowBack fontSize="large" />
      </div>
      <div className="photoDetails-container">
        <img
          className="image-container"
          src="https://i.pinimg.com/564x/ce/81/cb/ce81cb6fc52a2fe9a5ba221204ce4b33.jpg"
          alt="welcome to Authentik"
        />

        <div className="details-container">
          <div className="content-container">
            <div className="options">
              <div className="option-item">
                <div className="option-item-icon">
                  <MoreHoriz />
                </div>
                <div className="option-item-icon">
                  <IosShare />
                </div>
                <div className="option-item-icon">
                  <FontAwesomeIcon icon={faLink} />
                </div>
              </div>
              <div className="option-item">
                <button className="option-item-button-one">
                  My Saves <KeyboardArrowDown />
                </button>
                <button className="option-item-button-two">Save</button>
              </div>
            </div>

            <a
              href="https://react-portfolio-virid-sigma.vercel.app/"
              target="_blank"
              className="profile-link"
              rel="noreferrer"
            >
              ronie.timbal
            </a>
            <h1 className="title">
              Authentik landing page design inspiration -Lapa Ninja
            </h1>
            <p className="description">
              We help creative entrepreneurs build an honest brand & digital
              platform.
            </p>
            <div className="profile-info">
              <div className="profile-left-section">
                <img
                  src="https://i.pinimg.com/75x75_RS/d8/35/58/d83558a8c8eaf48d915ebe13ef394424.jpg"
                  width="50px"
                  height="50px"
                  alt="profile-icon"
                  className="profile-icon"
                />
                <div className="profile-details">
                  <h6>Lapa Ninja</h6>
                  <p>10.9k followers</p>
                </div>
              </div>
              <div>
                <button className="follow-button">Follow</button>
              </div>
            </div>
            <h5 className="comments">Comments</h5>
            <p className="comment-description">
              No comments yet! Add one to start the conversation.
            </p>
          </div>
          <div className="comment-section">
            <div className="comment-section-item-one">
              <AccountCircle fontSize="large" />
            </div>
            <div className="comment-section-item-two">
              <input
                type="text"
                placeholder="Add a comment"
                className="comment-input"
              />
              <img src={emoji} alt="emoji" className="emoji" />
            </div>
          </div>
        </div>
      </div>
      <div className="help">
        <QuestionMarkOutlined fontSize="large" />
      </div>
      <h4 style={{ textAlign: "center", padding: "20px" }}>More like this</h4>
      <div className="display-image-row">{displayImage}</div>
    </div>
  );
}
