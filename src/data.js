const imageData = [
  {
    id: 1,
    image:
      "https://i.pinimg.com/564x/0a/3b/a7/0a3ba78a8c01c21fc15ce66919b48516.jpg",
  },
  {
    id: 2,
    image:
      "https://i.pinimg.com/564x/9b/c9/df/9bc9dff8b895c325de10bd5c24b1ae56.jpg",
  },
  {
    id: 3,
    image:
      "https://i.pinimg.com/564x/74/73/58/74735853d2439f9cbc4a14e9bc39299a.jpg",
  },
  {
    id: 4,
    image:
      "https://i.pinimg.com/564x/22/13/af/2213afdb7def7734b36ecc271f46330e.jpg",
  },

  {
    id: 5,
    image:
      "https://i.pinimg.com/564x/02/b2/0e/02b20e37ea3199186bdc9c70433f1518.jpg",
  },
  {
    id: 6,
    image:
      "https://i.pinimg.com/564x/f0/73/31/f073318eb63772de3dc893adc80547c6.jpg",
  },
  {
    id: 7,
    image:
      "https://i.pinimg.com/564x/a6/25/70/a6257088537d85c92cded9e98fdcf283.jpg",
  },

  {
    id: 8,
    image:
      "https://i.pinimg.com/564x/28/e3/49/28e349abd9d3bd921eac67eb298eac59.jpg",
  },
];

export default imageData;
